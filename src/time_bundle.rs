use std::marker::PhantomData;

use num_traits::{Float, FromPrimitive};
use specs_bundler::{Bundle, Bundler};

use super::{Time, TimeSystem};

pub struct TimeBundle<'deps, T> {
    pub deps: Vec<&'deps str>,
    _marker: PhantomData<T>,
}

impl<'deps, T> Default for TimeBundle<'deps, T> {
    #[inline]
    fn default() -> Self {
        TimeBundle::new(&[])
    }
}

impl<'deps, T> TimeBundle<'deps, T> {
    #[inline]
    pub fn new(deps: &[&'deps str]) -> Self {
        TimeBundle {
            deps: deps.to_vec(),
            _marker: PhantomData,
        }
    }
}

impl<'deps, 'world, 'a, 'b, T> Bundle<'world, 'a, 'b> for TimeBundle<'deps, T>
where
    T: 'static + Sync + Send + Float + FromPrimitive,
{
    type Error = ();

    #[inline]
    fn bundle(
        self,
        mut bundler: Bundler<'world, 'a, 'b>,
    ) -> Result<Bundler<'world, 'a, 'b>, Self::Error> {
        bundler.world.insert(Time::<T>::default());

        bundler.dispatcher_builder.add(
            TimeSystem::<T>::new(),
            TimeSystem::<T>::name(),
            self.deps.as_slice(),
        );

        Ok(bundler)
    }
}
