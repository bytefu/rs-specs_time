use std::any::type_name;
use std::marker::PhantomData;

use num_traits::{Float, FromPrimitive};
use specs::{System, Write};

use super::Time;

pub struct TimeSystem<T>(PhantomData<T>);

impl<T> Default for TimeSystem<T> {
    #[inline(always)]
    fn default() -> Self {
        TimeSystem(PhantomData)
    }
}

impl<T> TimeSystem<T> {
    #[inline(always)]
    pub fn new() -> Self {
        TimeSystem(PhantomData)
    }
}

impl<T> TimeSystem<T> {
    #[inline(always)]
    pub fn name() -> &'static str {
        type_name::<Self>()
    }
}

impl<'a, T> System<'a> for TimeSystem<T>
where
    T: 'static + Send + Sync + Float + FromPrimitive,
{
    type SystemData = Write<'a, Time<T>>;

    #[inline]
    fn run(&mut self, mut time: Self::SystemData) {
        time.update();
    }
}

#[cfg(test)]
mod test {
    use super::super::TimeBundle;
    use super::*;

    use std::{thread, time};

    use specs::{DispatcherBuilder, World};
    use specs_bundler::Bundler;

    #[test]
    fn test_system() {
        let mut world = World::empty();

        let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
            .bundle(TimeBundle::<f64>::default())
            .unwrap()
            .build();

        for _ in 0..60 {
            dispatcher.dispatch(&world);
            thread::sleep(time::Duration::from_millis(16));
        }

        let time = world.fetch::<Time<f64>>();
        assert_eq!(time.frame(), 60);
        assert_eq!((time.current() + 0.5) as usize, 1);
    }

    #[test]
    fn test_time_system_name() {
        assert_eq!(
            TimeSystem::<f64>::name(),
            "specs_time::time_system::TimeSystem<f64>"
        );

        struct Example;
        assert_eq!(
            TimeSystem::<Example>::name(),
            "specs_time::time_system::TimeSystem<specs_time::time_system::test::test_time_system_name::Example>"
        );
    }
}
